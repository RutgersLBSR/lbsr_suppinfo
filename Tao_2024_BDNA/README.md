# Tao_2024_BDNA



## Introduction

This directory contains input coordinates and parameter files for the B-DNA system with a G:T wobble mismatch.


Simulation data of this system has appeared in one-or-more manuscripts.


- Y. Tao, T. J. Giese, S. Ekesan, J. Zeng, B. Aradi, B. Hourahine, H. M. Aktulga, A. Gotz, K. M. Merz Jr., and D. M. York, "Amber free energy tools: Interoperable software for free energy simulations using generalized quantum mechanical/molecular mechanical and machine learning potentials" in prep.

- Y. Tao, T. J. Giese, H. M. Al-Hashimi, and D. M. York, "Electronic and nuclear quantum effects on proton transfer reactions of guanine-thymine (G-T) mispairs using combined quantum mechanical/molecular mechanical and machine learning potentials" in prep.


## Files

- [ ] bdna.parm7: Amber parameter file
- [ ] wGT.rst7 and wGT.disang: Coordinates corresponding to the W:G wobble basepair
- [ ] GsT.rst7 and GsT.disang: Coordinates corresponding to the G*:T tautomer
- [ ] GTs.rst7 and GTs.disang: Coordinates corresponding to the G:T* tautomer
- [ ] template.mdin: Template sander input file. Requires AmberTools 2024 or later.
- [ ] dprc_model1.pb, dprc_model2.pb, dprc_model3.pb, dprc_model4.pb: The 4 DPRc neural network parameter files.

