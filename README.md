# LBSR_SuppInfo


## Introduction

This repository contains Amber parameters, coordinate files, example input files, and neural network parameters to accompany published work.


## Tao_2024_BDNA

Rare tautomeric forms of nucleobases can lead to Watson-Crick-like (WC-like) mispairs in DNA, but the process of proton transfer is fast and difficult to detect experimentally. NMR studies show the evidence of short time existence for WC-like guanine-thymine (G-T) mispairs, however the mechanism of proton transfer and the degree to which nuclear quantum effects play a role are unclear. We use a B-DNA helix exhibiting a wGT (wobble G:T) mispair as a model system to study the tautomerization reactions.




